export interface BaseFilterEntity {
  page: number
  limit: number
  q?: string
}
